app.controller('ProductsController', function ($scope, $http) {
    GetList();
    
   
    function GetList () {
        $http({
            url: 'http://localhost/ngjs/index.php/products/',
            method: "GET",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(function (response) {
            $scope.itens = response.data;
        });
    }

    $scope.SaveProduct = function() {
        
        $http({
            url: 'http://localhost/ngjs/index.php/products/add',
            method: "POST",
            data: $scope.form,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(function (response) {
            if ( response.status == 200 ) {
                let data_form = $scope.form;
                data_form['id'] = response.data;
                $scope.itens.push(data_form);
                swal({
                    type: "success",
                    text : "Produto inserido com sucesso!"
                });
                $('.modal').modal('hide');
            } else {
                swal({
                    type: "error",
                    text: "Não foi possivel inserir o produto, tente novamente!"
                });
            }
        });
    }

    $scope.EditProduct = function (product_id) {
        $http({
            url: 'http://localhost/ngjs/index.php/products/find/'+product_id,
            method: "POST",
            data: $scope.form,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(function (response) {
            $scope.formedit = {
                id: response.data.id,
                name: response.data.name,
                amount: parseInt(response.data.amount)
            };
        });
    }

    $scope.UpdateProduct = function () {
        console.log($scope.formedit);
        $http({
            url: 'http://localhost/ngjs/index.php/products/update/',
            method: "POST",
            data: $scope.formedit,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
        .then(function (response) {
            if (response.status == 200) {
                GetList();
                swal({
                    type: "success",
                    text: "Produto atualizado com sucesso!"
                });
                $('.modal').modal('hide');
            } else {
                swal({
                    type: "error",
                    text: "Não foi possivel atualizar o produto, tente novamente!"
                });
            }
        });
    }

    $scope.DeleteProduct = function (product_id) {

        swal({
            title: 'Tem certeza que deseja excluir?',
            text: "Esta operação é irreversivel!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $http({
                    url: 'http://localhost/ngjs/index.php/products/delete/',
                    method: "POST",
                    data: {id : product_id},
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                })
                .then(function (response) {
                    if (response.status == 200) {
                        GetList();
                        swal({
                            type: "success",
                            text: "Produto deletado com sucesso!"
                        });
                    } else {
                        swal({
                            type: "error",
                            text: "Não foi possivel deletado o produto, tente novamente!"
                        });
                    }
                });
            }
        })
    }
});