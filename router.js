app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: 'app/index.html',
        })
        .when("/products", {
            templateUrl: 'app/Products.html',
            controller: "ProductsController"
        })
});